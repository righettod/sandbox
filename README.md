# 🧪 Sandbox 🧪

## 🎯 Objectives

> **Note**: Web content is contained and exposed via the folder [docs](docs).

Provide the following elements during online training web challenges/labs.

* A web server supporting HTTP and HTTPS protocols:
  * HTTPS via GitHub pages features.
  * HTTP via [THC disposable server](https://blog.thc.org/disposable-root-servers) instance.
* A network listener via a [THC disposable server](https://blog.thc.org/disposable-root-servers) instance.

